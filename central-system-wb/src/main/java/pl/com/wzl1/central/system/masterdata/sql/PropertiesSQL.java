package pl.com.wzl1.central.system.masterdata.sql;

/*
 *  ************************************************************
 *  * SYSTEM CENTRALNY
 *  *
 *  * Autorskie Prawa Majatkowe:
 *  * Wojskowe Zaklady Lacznosci Nr 1 w Zegrzu (WZL1)
 *  ************************************************************
 *  * Autor: Artur Kowalski
 *  * Data utworzenia: 02.08.2017 12:06
 *  * Wersja: 0.1
 *  ************************************************************
 *
 */

public class PropertiesSQL {


    public static String updateProperty = " UPDATE  PARAM SET"
            + " VALUE = :value"
            + ", ACTIVE = :active"
            + " WHERE CODE = :code";

    public static String getAllProperties = "SELECT " +
            "P.DICT_PATP TYPE, P.DICT_PAUN UNIT, P.NAME, P.VALUE, P.DESCR DESCRIPTION, P.CODE, P.ACTIVE " +
            "FROM PARAM P";

    public static String getValue = "SELECT " +
            "VALUE " +
            "FROM " +
            "PARAM " +
            "WHERE " +
            "CODE = :code";

    public static String getProperty = "SELECT " +
            "P.DICT_PATP TYPE, P.DICT_PAUN UNIT, P.NAME, P.VALUE, P.DESCR DESCRIPTION, P.CODE, P.ACTIVE " +
            "FROM " +
            "PARAM P " +
            "WHERE " +
            "CODE = :code";
}
