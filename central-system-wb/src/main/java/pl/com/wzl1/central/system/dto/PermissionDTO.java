
/************************************************************
 * SYSTEM CENTRALNY
 *
 * Autorskie Prawa Majątkowe:
 * Wojskowe Zakłady Łączności Nr 1 w Zegrzu (WZŁ1)
 ************************************************************
 * Autor: Rafał Karczmarz 
 * Data utworzenia: 14 lip 2017
 * Wersja: 0.1
 ************************************************************/

package pl.com.wzl1.central.system.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor

public class PermissionDTO {
    private String name;

    private String descr;

    private String code;

    private String userCre;

    private Date dateCre;

    private String userMod;

    private Date dateMod;
}
