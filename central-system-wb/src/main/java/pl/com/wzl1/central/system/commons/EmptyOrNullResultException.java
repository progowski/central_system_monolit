package pl.com.wzl1.central.system.commons;

/*
 *  ************************************************************
 *  * SYSTEM CENTRALNY
 *  *
 *  * Autorskie Prawa Majatkowe:
 *  * Wojskowe Zaklady Lacznosci Nr 1 w Zegrzu (WZL1)
 *  ************************************************************
 *  * Autor: Artur Kowalski
 *  * Data utworzenia: 20.10.2017 07:46
 *  * Wersja: 0.1
 *  ************************************************************
 *
 */
public class EmptyOrNullResultException  extends RuntimeException {
    public EmptyOrNullResultException() {
    }

    public EmptyOrNullResultException(String message) {
        super(message);
    }
}