/*
 ***********************************************************
 * SYSTEM CENTRALNY
 *
 * Autorskie Prawa Majątkowe:
 * Wojskowe Zakłady Łączności Nr 1 w Zegrzu (WZŁ1)
 ************************************************************
 * Autor: Piotr Rogowski
 * Data utworzenia: 08.08.2017 11:10:12
 * Wersja: 0.1
 ***********************************************************/

package pl.com.wzl1.central.system.masterdata.controller;

import pl.com.wzl1.central.system.dto.ResponseException;
import pl.com.wzl1.central.system.dto.ResponseStatusDTO;

public class MainController {

    public static ResponseStatusDTO convertToResponDTO(ResponseException re) {

        if (re != null) {
            return new ResponseStatusDTO((re.getStatus() != null ? re.getStatus().toString() : null), re.getCode(), re
                    .getMessage());
        }

        return null;
    }
}
