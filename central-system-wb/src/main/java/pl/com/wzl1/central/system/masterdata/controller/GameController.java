/*
***********************************************************
 * SYSTEM CENTRALNY
 * 
 * Autorskie Prawa Majątkowe:
 * Wojskowe Zakłady Łączności Nr 1 w Zegrzu (WZŁ1)
 ************************************************************
 * Autor: Piotr Rogowski 
 * Data utworzenia: 08.08.2017 11:10:12
 * Wersja: 0.1
 ***********************************************************/

package pl.com.wzl1.central.system.masterdata.controller;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import pl.com.wzl1.central.system.dto.GameMapDTO;
import pl.com.wzl1.central.system.masterdata.service.GameSrv;

import javax.websocket.server.PathParam;
import java.util.List;

@RestController
@Api(value = "Game Resource")
@RequestMapping("/wzl/v1/game")
public class GameController {

    @Autowired
    private GameSrv gameSrv;

    @ApiOperation(value = "Game ident from central system")
    @ApiResponses(value =
            { @ApiResponse(code = 200, message = "Successful",response=Boolean.class),
                    @ApiResponse(code = 500, message = "INSIDE SERVER ERROR") })
    @ApiImplicitParams({
            @ApiImplicitParam(name = "Authorization", value = "Authorization token",
                    required = true, dataType = "string", paramType = "header")
    })
    @HystrixCommand(commandKey = "DictControllerImpl.list")
    @RequestMapping(value = "/id", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.ALL_VALUE)
    public Integer gameId(@PathParam("gameEgmId") Integer gameEgmId, @PathParam("version") String version ) {
        System.err.println("gameEgmId = "+gameEgmId);
        System.err.println("version = "+version);
        return gameSrv.getGameId(gameEgmId,version);
    }

    @ApiOperation(value = "List games with idents,versions")
    @ApiResponses(value =
            { @ApiResponse(code = 200, message = "Successful",response=Boolean.class),
                    @ApiResponse(code = 500, message = "INSIDE SERVER ERROR") })
    @ApiImplicitParams({
            @ApiImplicitParam(name = "Authorization", value = "Authorization token",
                    required = true, dataType = "string", paramType = "header")
    })
    @HystrixCommand(commandKey = "DictControllerImpl.gameListMap")
    @RequestMapping(value = "/map/list", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.ALL_VALUE)
    public List<GameMapDTO> gameListMap() {
        return gameSrv.listGame();
    }

}
