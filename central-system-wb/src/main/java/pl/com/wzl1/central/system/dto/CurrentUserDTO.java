package pl.com.wzl1.central.system.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/************************************************************
 * SYSTEM CENTRALNY
 *
 * Autorskie Prawa Majątkowe: Wojskowe
 * Zakłady Łączności Nr 1 w Zegrzu (WZŁ1)
 ************************************************************
 * Autor: Rafał Karczmarz
 * Data utworzenia: 09 sie 2017 Wersja: 0.1
 ************************************************************/
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CurrentUserDTO {

    private String login;

    private String name;

    private String surname;

    private Boolean active;

    private Boolean blocked;

    private Date blockedTo;

    private Boolean deleted;

    private String password;

    private String dictUsgr;

    private String depCode;

    private List<PermissionDTO> permission = new ArrayList<PermissionDTO>();

    public boolean isBlockedTemporary() {
        if (blockedTo != null) {
            LocalDate date = blockedTo.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
            if (date.isAfter(LocalDate.now())) {
                return true;
            }
        }
        return false;
    }
}
