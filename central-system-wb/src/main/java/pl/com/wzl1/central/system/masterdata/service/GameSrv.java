
/*
***********************************************************
 * SYSTEM CENTRALNY
 * 
 * Autorskie Prawa Majątkowe:
 * Wojskowe Zakłady Łączności Nr 1 w Zegrzu (WZŁ1)
 ************************************************************
 * Autor: Piotr Rogowski 
 * Data utworzenia: 10.08.2017 13:15:01
 * Wersja: 0.1
 ***********************************************************/

package pl.com.wzl1.central.system.masterdata.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.com.wzl1.central.system.dto.GameMapDTO;
import pl.com.wzl1.central.system.masterdata.dao.GameDao;

import java.util.List;

@Service
public class GameSrv {

    @Autowired
    private GameDao gameDao;

    public Integer getGameId(Integer gameEgmId, String version) {
        if (gameEgmId != null && version != null) {
            return gameDao.getGameId(gameEgmId,version);
        }
        return null;
    }

    public List<GameMapDTO> listGame() {
        return gameDao.listGame();
    }
}
