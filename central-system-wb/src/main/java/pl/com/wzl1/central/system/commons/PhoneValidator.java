package pl.com.wzl1.central.system.commons.dto.validator;

public class PhoneValidator {
    public static boolean validatePhoneNumber(String phoneNo) {
        phoneNo = phoneNo.replaceAll("-", "");

        if (phoneNo.matches("\\d{11}"))
            return true;
        else
            return false;

    }
}
