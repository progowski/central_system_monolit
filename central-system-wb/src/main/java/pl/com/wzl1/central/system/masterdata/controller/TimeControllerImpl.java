package pl.com.wzl1.central.system.masterdata.controller;


import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import pl.com.wzl1.central.system.commons.TimeService;

@RestController
@RequestMapping("/wzl/v1")
public class TimeControllerImpl {
    private final TimeService timeService;

    public TimeControllerImpl(TimeService timeService) {
        this.timeService = timeService;
    }


    @HystrixCommand(commandKey = "TimeControllerImpl.getTime")
    @RequestMapping(value = "/time", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.ALL_VALUE)
    public Long getTime() {
        return timeService.getTime();
    }

}
