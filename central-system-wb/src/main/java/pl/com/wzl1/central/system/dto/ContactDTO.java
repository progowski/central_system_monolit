
/*
***********************************************************
 * SYSTEM CENTRALNY
 * 
 * Autorskie Prawa Majątkowe:
 * Wojskowe Zakłady Łączności Nr 1 w Zegrzu (WZŁ1)
 ************************************************************
 * Autor: Piotr Rogowski 
 * Data utworzenia: 20.09.2017 09:52:11
 * Wersja: 0.1
 ***********************************************************/

package pl.com.wzl1.central.system.dto;

public class ContactDTO {

    private String code;
    private String type;
    private String target;

    public ContactDTO() {
        super();
    }

    public ContactDTO(String code, String type, String target) {
        super();
        this.code = code;
        this.type = type;
        this.target = target;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    @Override
    public String toString() {
        return "ContactDTO [code=" + code + ", type=" + type + ", target=" + target + "]";
    }

}
