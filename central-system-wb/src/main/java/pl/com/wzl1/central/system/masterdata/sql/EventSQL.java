
/*
 ***********************************************************
 * SYSTEM CENTRALNY
 *
 * Autorskie Prawa Majątkowe:
 * Wojskowe Zakłady Łączności Nr 1 w Zegrzu (WZŁ1)
 ************************************************************
 * Autor: Piotr Rogowski
 * Data utworzenia: 01.09.2017 08:21:46
 * Wersja: 0.1
 ***********************************************************/


package pl.com.wzl1.central.system.masterdata.sql;

public class EventSQL {

    public static final String LIST_EVENTS = "SELECT "
            + "E.DICT_EVNT code,"
            + "EN.VALUE name,"
            + "EVCL.VALUE priorityName, "
            + "EVTP.VALUE typeName,"
            + "EVCO.VALUE colorName,"
            + "E.SOUND sound,"
            + "E.LIFE_TIME lifeTime "
            + "FROM EVENT_CONF E "
            + "LEFT JOIN DICT_ELEM EVCL ON EVCL.CODE = E.DICT_EVCL "
            + "LEFT JOIN DICT_ELEM EVTP ON EVTP.CODE = E.DICT_EVTP "
            + "LEFT JOIN DICT_ELEM EVCO ON EVCO.CODE = E.DICT_EVCO "
            + "LEFT JOIN DICT_ELEM EN ON EN.CODE = E.DICT_EVNT";


    public static final String EVENT_PROPERTIES = "SELECT "
            + "E.DICT_EVNT code,"
            + "EN.VALUE name,"
            + "EN.DESCR description,"
            + "E.DICT_EVCL priorityCode,"
            + "EVCL.VALUE priorityName, "
            + "E.DICT_EVTP typeCode,"
            + "EVTP.VALUE typeName,"
            + "E.DICT_EVCO colorCode,"
            + "EVCO.VALUE colorName,"
            + "E.SOUND sound, "
            + "E.LIFE_TIME lifeTime,"
            + "E.TOP top "
            + "FROM EVENT_CONF E "
            + "LEFT JOIN DICT_ELEM EVCL ON EVCL.CODE = E.DICT_EVCL "
            + "LEFT JOIN DICT_ELEM EVTP ON EVTP.CODE = E.DICT_EVTP "
            + "LEFT JOIN DICT_ELEM EVCO ON EVCO.CODE = E.DICT_EVCO "
            + "LEFT JOIN DICT_ELEM EN ON EN.CODE = E.DICT_EVNT "
            + "WHERE E.DICT_EVNT = :code";

    public static final String LIST_EVENT_MESSAGE = "SELECT "
            + "TYP type,"
            + "TARGET target "
            + "FROM EVENT_MESSAGE_CONF "
            + "WHERE DICT_EVNT = :code";

    public static final String UPDATE_EVENT = "UPDATE EVENT_CONF SET "
            + "USER_MOD = :user,"
            + "DATE_MOD = SYSDATE,"
            + "DICT_EVCO = :color_code,"
            + "DICT_EVTP = :type_code,"
            + "DICT_EVCL = :priority_code,"
            + "SOUND = :sound,"
            + "TOP = :top, "
            + "DICT_EVNT = :code, "
            + "LIFE_TIME = :life_time "
            + "WHERE DICT_EVNT = :code "
            + "AND NOT EXISTS (SELECT E.DICT_EVNT FROM EVENT_CONF E "
            + "WHERE NVL(E.DICT_EVCO,'') = NVL(:color_code,'') AND NVL(E.DICT_EVTP,'') = NVL(:type_code,'') AND NVL(E.DICT_EVCL,'') = NVL(:priority_code,'') "
            + "AND NVL(E.SOUND,-1) = NVL(:sound,-1) AND NVL(E.TOP,-1) = NVL(:top,-1) AND NVL(E.LIFE_TIME,-1) = NVL(:life_time,-1) AND E.DICT_EVNT = :code)";

//    public static final String REMOVE_EVENT_MESSAGES = "DELETE FROM EVENT_MESSAGE WHERE EVENT = :code";

    public static final String ADD_EVENT_MESSAGE = "INSERT INTO EVENT_MESSAGE_CONF"
            + "(USER_CRE,DATE_CRE,DICT_EVNT,TYP,TARGET) "
            + "VALUES(:user,SYSDATE,:code,:type,:target)";

//    public static final String ACTIVE_EVENT_MESSAGE = "UPDATE EVENT_MESSAGE_CONF "
//                    + "SET ACTIVE = 1,"
//                    + "USER_MOD = :user,"
//                    + "DATE_MOD = SYSDATE "
//                    + "WHERE TARGET = :target "
//                    + "AND DICT_EVNT = :code";

    public static final String DISACTIVE_EVENT_MESSAGE = "DELETE FROM EVENT_MESSAGE_CONF "
            + "WHERE TARGET NOT IN (:targets) "
            + "AND DICT_EVNT = :code";

    public static final String DISACTIVE_ALL_EVENT_MESSAGE = "DELETE FROM EVENT_MESSAGE_CONF "
            + "WHERE DICT_EVNT = :code";

    public static final String LIST_EVENTS_ALL_INFO = "SELECT "
            + "E.DICT_EVNT code,"
            + "EN.VALUE name,"
            + "EN.DESCR description,"
            + "E.DICT_EVCL priorityCode,"
            + "EVCL.VALUE priorityName, "
            + "E.DICT_EVTP typeCode,"
            + "EVTP.VALUE typeName,"
            + "E.DICT_EVCO colorCode,"
            + "EVCO.VALUE colorName,"
            + "E.SOUND sound, "
            + "E.LIFE_TIME lifeTime,"
            + "E.TOP top "
            + "FROM EVENT_CONF E "
            + "LEFT JOIN DICT_ELEM EVCL ON EVCL.CODE = E.DICT_EVCL "
            + "LEFT JOIN DICT_ELEM EVTP ON EVTP.CODE = E.DICT_EVTP "
            + "LEFT JOIN DICT_ELEM EVCO ON EVCO.CODE = E.DICT_EVCO "
            + "LEFT JOIN DICT_ELEM EN ON EN.CODE = E.DICT_EVNT";

    public static final String LIST_CONTACTS_EVENTS = "SELECT "
            + "EC.DICT_EVNT code,"
            + "EM.TYP type,"
            + "EM.TARGET target "
            + "FROM EVENT_CONF EC "
            + "JOIN EVENT_MESSAGE_CONF EM ON EM.DICT_EVNT = EC.DICT_EVNT ";


    public static final String SET_DATE_MOD_EVENT = "UPDATE EVENT_CONF "
            + "SET USER_MOD = :user,"
            + "DATE_MOD = SYSDATE "
            + "WHERE DICT_EVNT = :code";

}
