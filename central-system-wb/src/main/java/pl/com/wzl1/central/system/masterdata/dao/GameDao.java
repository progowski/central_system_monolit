package pl.com.wzl1.central.system.masterdata.dao;

import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;
import pl.com.wzl1.central.system.dto.GameMapDTO;
import pl.com.wzl1.central.system.masterdata.sql.GameSQL;

import java.sql.Types;
import java.util.List;

@Repository
public class GameDao {

    private final NamedParameterJdbcTemplate jdbcTemplate;

    public GameDao(NamedParameterJdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public Integer getGameId(Integer gameEgmId, String version) {
        MapSqlParameterSource mapParams = new MapSqlParameterSource();
        mapParams.addValue("gameEgmId", gameEgmId, Types.INTEGER);
        mapParams.addValue("version", version, Types.VARCHAR);
        try {
            return jdbcTemplate.queryForObject(GameSQL.GAME_ID, mapParams, Integer.class);
        } catch (EmptyResultDataAccessException e) {
            return null;
        }
    }

    public List<GameMapDTO> listGame() {
        MapSqlParameterSource mapParams = new MapSqlParameterSource();
        return jdbcTemplate.query(GameSQL.GAME_LIST, mapParams, new BeanPropertyRowMapper<GameMapDTO>(GameMapDTO.class));
    }
}
