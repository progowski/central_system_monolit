package pl.com.wzl1.central.system.dto;

import lombok.Data;
import pl.com.wzl1.central.system.dto.ResponseStatusDTO;

        /*
 *  ************************************************************
 *  * SYSTEM CENTRALNY
 *  *
 *  * Autorskie Prawa Majatkowe:
 *  * Wojskowe Zaklady Lacznosci Nr 1 w Zegrzu (WZL1)
 *  ************************************************************
 *  * Autor: Artur Kowalski
 *  * Data utworzenia: 20.10.2017 07:46
 *  * Wersja: 0.1
 *  ************************************************************
 *
 */



@Data
public class ResponseException extends RuntimeException {
    String status;
    String code;
    String description;

    public ResponseException(String status) {
        this.status = status;
    }

    public ResponseException(String status, String code, String description) {
        super(description);
        this.status = status;
        this.code = code;
        this.description = description;
    }

    public ResponseStatusDTO getResponse(){
        return new ResponseStatusDTO(status,code,description);
    }

    public String getStatus() {
        return status;
    }

    public String getCode() {
        return code;
    }

    public String getDescription() {
        return description;
    }

    @Override
    public String toString() {
        return "ResponseException{" +
                "status='" + status + '\'' +
                ", code='" + code + '\'' +
                ", description='" + description + '\'' +
                '}';
    }
}
