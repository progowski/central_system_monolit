package pl.com.wzl1.central.system.commons.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum LoginStatusEnum {

    LOGIN_BLOCK_TO("USOP_BLOCK", "Zablokowanie użytkownika na określony czas", "login.failure.set.user.blockedTo."),
    LOGIN_LOGON("USOP_LOGIN", "Zalogowanie użytkownika", null),
    LOGIN_LOGOUT("USOP_LOGOU", "Wylogowanie użytkownika", null),
    LOGIN_LOG_BLOCK("USOP_LOGBL", "Próba zalogowania przez zablokowanego użytkownika trwale", "login.failure.user.blocked"),
    LOGIN_LOG_BLOCK_TO("USOP_LOGBT", "Próba zalogowania przez zablokowanego czasowo użytkownika ", "login.failure.user.blockedTo."),
    LOGIN_BAD_PASSWORD("USOP_LOGFP", "Nieprawidłowe hasło podczas logowania użytkownika","login.failure.bad.password"),
    LOGIN_BAD_LOGIN("USOP_LOGFL", "Nieprawidłowy login podczas logowania użytkownika", "login.failure.not.exists"),
    LOGIN_NO_ACTIVE("USOP_LOGNA","Próba zalogowania przez użytkownika nie aktywowanego","adm.user.validate.bad.user.noActive");

    private final String code;
    private final String description;
    private final String exceptionCode;
}
