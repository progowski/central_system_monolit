package pl.com.wzl1.central.system.masterdata.sql;

public class GameSQL {
    public static final String GAME_ID = "SELECT ID FROM GAME WHERE DEV_ID = :gameEgmId AND VER = :version";

    public static final String GAME_LIST = "SELECT ID, DEV_ID, VER FROM GAME";
}
