package pl.com.wzl1.central.system.masterdata.dao;

/*
 *  ************************************************************
 *  * SYSTEM CENTRALNY
 *  *
 *  * Autorskie Prawa Majatkowe:
 *  * Wojskowe Zaklady Lacznosci Nr 1 w Zegrzu (WZL1)
 *  ************************************************************
 *  * Autor: Artur Kowalski
 *  * Data utworzenia: 02.08.2017 13:57
 *  * Wersja: 0.1
 *  ************************************************************
 *
 */

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;
import pl.com.wzl1.central.system.dto.PropertyDTO;
import pl.com.wzl1.central.system.masterdata.model.Property;
import pl.com.wzl1.central.system.masterdata.sql.PropertiesSQL;

import java.sql.Types;
import java.util.List;


@Repository
public class PropertiesDaoImpl {

    private final NamedParameterJdbcTemplate jdbcTemplate;

    public PropertiesDaoImpl(NamedParameterJdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @SuppressWarnings("unchecked")
    public List<Property> getAll() {
        return jdbcTemplate.query(PropertiesSQL.getAllProperties, new BeanPropertyRowMapper<Property>(Property.class));
    }

    public void updateProperty(PropertyDTO propertyDTO) {

        int update = jdbcTemplate.update(PropertiesSQL.updateProperty,
                new MapSqlParameterSource()
                        .addValue("value", propertyDTO.getValue(), Types.INTEGER)
                        .addValue("active", propertyDTO.getActive(), Types.BOOLEAN)
                        .addValue("code", propertyDTO.getCode(), Types.VARCHAR));


        if (update == 0)
            throw new IllegalArgumentException();
    }

    public String getValue(String code) {

        String value = jdbcTemplate.queryForObject(PropertiesSQL.getValue,
                new MapSqlParameterSource()
                        .addValue("code", code, Types.VARCHAR)
                , String.class);

        if (value.equals("0"))
            throw new IllegalArgumentException();

        return value;

    }

    public Property getProperty(String code) {

        return jdbcTemplate.queryForObject(PropertiesSQL.getProperty,
                new MapSqlParameterSource()
                        .addValue("code", code, Types.VARCHAR)
                , new BeanPropertyRowMapper<Property>(Property.class));
    }
}
