package pl.com.wzl1.central.system.dto;

/*
 *  ************************************************************
 *  * SYSTEM CENTRALNY
 *  *
 *  * Autorskie Prawa Majatkowe:
 *  * Wojskowe Zaklady Lacznosci Nr 1 w Zegrzu (WZL1)
 *  ************************************************************
 *  * Autor: Artur Kowalski
 *  * Data utworzenia: 07.08.2017 12:29
 *  * Wersja: 0.1
 *  ************************************************************
 *
 */

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PropertyValueDTO {

    String value;
}
