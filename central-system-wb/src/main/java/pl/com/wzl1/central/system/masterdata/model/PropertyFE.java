package pl.com.wzl1.central.system.masterdata.model;

import lombok.Data;

@Data
public class PropertyFE {

    String name;

    String value;

    String description;

    DictElem type;

    DictElem unit;

    String code;

    Boolean active;
}