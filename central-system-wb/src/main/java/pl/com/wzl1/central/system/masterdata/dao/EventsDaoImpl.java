
/*
 ***********************************************************
 * SYSTEM CENTRALNY
 *
 * Autorskie Prawa Majątkowe:
 * Wojskowe Zakłady Łączności Nr 1 w Zegrzu (WZŁ1)
 ************************************************************
 * Autor: Piotr Rogowski
 * Data utworzenia: 01.09.2017 08:09:46
 * Wersja: 0.1
 ***********************************************************/

package pl.com.wzl1.central.system.masterdata.dao;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;
import pl.com.wzl1.central.system.dto.EventDTO;
import pl.com.wzl1.central.system.dto.EventMessageDTO;
import pl.com.wzl1.central.system.masterdata.model.ContactEvent;
import pl.com.wzl1.central.system.masterdata.model.Event;
import pl.com.wzl1.central.system.masterdata.model.EventMessage;
import pl.com.wzl1.central.system.masterdata.sql.EventSQL;

import java.sql.Types;
import java.util.List;
import java.util.Set;

@Repository
public class EventsDaoImpl {

    private final static Logger Logger = LogManager.getLogger(EventsDaoImpl.class);

    private final NamedParameterJdbcTemplate jdbcTemplate;

    public EventsDaoImpl(NamedParameterJdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public List<Event> getEvents() {
        return jdbcTemplate.query(EventSQL.LIST_EVENTS, new MapSqlParameterSource(),
                new BeanPropertyRowMapper<Event>(Event.class));
    }

    public Event getProperties(String codeEvent) {
        MapSqlParameterSource mapParams = new MapSqlParameterSource();
        mapParams.addValue("code", codeEvent, Types.VARCHAR);

        return jdbcTemplate.queryForObject(EventSQL.EVENT_PROPERTIES, mapParams,
                new BeanPropertyRowMapper<Event>(Event.class));
    }

    public List<EventMessage> getMessages(String codeEvent) {
        MapSqlParameterSource mapParams = new MapSqlParameterSource();
        mapParams.addValue("code", codeEvent, Types.VARCHAR);

        return jdbcTemplate.query(EventSQL.LIST_EVENT_MESSAGE, mapParams,
                new BeanPropertyRowMapper<EventMessage>(EventMessage.class));
    }

    public void updateEvent(EventDTO dto, String userEdit) {
        MapSqlParameterSource mapParams = new MapSqlParameterSource();
        mapParams.addValue("code", dto.getCode(), Types.VARCHAR);
        mapParams.addValue("user", userEdit, Types.VARCHAR);
        mapParams.addValue("color_code", dto.getColorCode(), Types.VARCHAR);
        mapParams.addValue("type_code", dto.getTypeCode(), Types.VARCHAR);
        mapParams.addValue("priority_code", dto.getPriorityCode(), Types.VARCHAR);
        mapParams.addValue("sound", dto.isSound() ? "1" : "0", Types.CHAR);
        mapParams.addValue("life_time", dto.getLifeTime(), Types.INTEGER);
        mapParams.addValue("top", (dto.getTop() != null && dto.getTop()) ? "1" : "0", Types.CHAR);

        jdbcTemplate.update(EventSQL.UPDATE_EVENT, mapParams);
    }

    public void deactivateMessages(Set<String> targets, String codeEvent, String userEdit) {
        System.err.println("targets = " + targets);
        System.err.println("codeEvent = " + codeEvent);

        MapSqlParameterSource mapParams = new MapSqlParameterSource();
        mapParams.addValue("code", codeEvent, Types.VARCHAR);
        mapParams.addValue("targets", targets, Types.VARCHAR);

        jdbcTemplate.update(EventSQL.DISACTIVE_EVENT_MESSAGE, mapParams);
    }

    public void addMessages(List<EventMessageDTO> list, String codeEvent, String userEdit) {
        for (EventMessageDTO dto : list) {
            MapSqlParameterSource mapParams = new MapSqlParameterSource();
            mapParams.addValue("code", codeEvent, Types.VARCHAR);
            mapParams.addValue("user", userEdit, Types.VARCHAR);
            mapParams.addValue("type", dto.getType(), Types.VARCHAR);
            mapParams.addValue("target", dto.getTarget(), Types.VARCHAR);

            try {
                jdbcTemplate.update(EventSQL.ADD_EVENT_MESSAGE, mapParams);
            } catch (org.springframework.dao.DuplicateKeyException e) {

            }
        }
    }

    public void deactivateAllMessages(String codeEvent, String userEdit) {
        MapSqlParameterSource mapParams = new MapSqlParameterSource();
        mapParams.addValue("code", codeEvent, Types.VARCHAR);

        jdbcTemplate.update(EventSQL.DISACTIVE_ALL_EVENT_MESSAGE, mapParams);
    }

    public List<Event> getEventsAllInfo() {
        return jdbcTemplate.query(EventSQL.LIST_EVENTS_ALL_INFO, new MapSqlParameterSource(),
                new BeanPropertyRowMapper<Event>(Event.class));
    }

    public List<ContactEvent> getContactEventList() {
        return jdbcTemplate.query(EventSQL.LIST_CONTACTS_EVENTS, new MapSqlParameterSource(),
                new BeanPropertyRowMapper<ContactEvent>(ContactEvent.class));
    }

    public void setDateModForEvent(String codeEvent, String userEdit) {
        MapSqlParameterSource mapParams = new MapSqlParameterSource();
        mapParams.addValue("code", codeEvent, Types.VARCHAR);
        mapParams.addValue("user", userEdit, Types.VARCHAR);

        jdbcTemplate.update(EventSQL.SET_DATE_MOD_EVENT, mapParams);
    }

}
