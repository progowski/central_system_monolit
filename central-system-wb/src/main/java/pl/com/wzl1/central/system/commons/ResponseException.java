
/************************************************************
 * SYSTEM CENTRALNY
 * 
 * Autorskie Prawa Majątkowe:
 * Wojskowe Zakłady Łączności Nr 1 w Zegrzu (WZŁ1)
 ************************************************************
 * Autor: Piotr Rogowski 
 * Data utworzenia: 2017-07-13
 * Wersja: 0.1
 ************************************************************/

package pl.com.wzl1.central.system.commons;

import java.io.Serializable;

public class ResponseException extends Exception implements Serializable {

    private String code;
    private Integer status;

    public ResponseException(String code, String message, Integer status) {
        super(message);
        this.code = code;
        this.status = status;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

}
