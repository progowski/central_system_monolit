package pl.com.wzl1.central.system.masterdata.service;

/*
 *  ************************************************************
 *  * SYSTEM CENTRALNY
 *  *
 *  * Autorskie Prawa Majatkowe:
 *  * Wojskowe Zaklady Lacznosci Nr 1 w Zegrzu (WZL1)
 *  ************************************************************
 *  * Autor: Artur Kowalski
 *  * Data utworzenia: 23.08.2017 14:55
 *  * Wersja: 0.1
 *  ************************************************************
 *
 */

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.com.wzl1.central.system.commons.EmptyOrNullResultException;
import pl.com.wzl1.central.system.commons.ObjectToDtoConverter;
import pl.com.wzl1.central.system.dto.DictElemDTO;
import pl.com.wzl1.central.system.dto.PropertyDTO;
import pl.com.wzl1.central.system.dto.PropertyFEDTO;
import pl.com.wzl1.central.system.dto.ResponseStatusDTO;
import pl.com.wzl1.central.system.masterdata.dao.PropertiesDaoImpl;
import pl.com.wzl1.central.system.masterdata.model.Property;

import java.util.List;
import java.util.Map;

import static java.util.stream.Collectors.toList;
import static org.apache.commons.lang.Validate.notEmpty;
import static org.springframework.util.StringUtils.isEmpty;

@Service
@Transactional
public class PropertiesSrvImpl {

    protected final static String TYPES_DICT_CODE = "PATP";
    protected final static String UNITS_DICT_CODE = "PAUN";
    private final static Logger LOGGER = LogManager.getLogger(PropertiesSrvImpl.class);
    private static int unitsMultiplier;
    private final PropertiesDaoImpl propertiesDao;

    private final DictSrvImpl dictSrv;
    private final ObjectToDtoConverter objectToDtoConverter;

    public PropertiesSrvImpl(PropertiesDaoImpl propertiesDao, DictSrvImpl dictSrv,ObjectToDtoConverter objectToDtoConverter) {
        this.propertiesDao = propertiesDao;
        this.dictSrv = dictSrv;
        this.objectToDtoConverter = objectToDtoConverter;
    }


    public List<PropertyFEDTO> getAllProperties() {

        LOGGER.info("==== Working...");

        debugLogging("====== Requesting DB for PropertyList...");
        List<Property> propertyList = propertiesDao.getAll();
        //TODO sprawdzenie czy nie jest puste i rzucenie wyjątku jeśli tak
        debugLogging("======== DB response: " + propertyList.toString());

        debugLogging("====== Requesting DB for types...");
        Map<String, DictElemDTO> types = dictSrv.getMapDictElem(TYPES_DICT_CODE);
        notEmpty(types);
        debugLogging("======== DB response: " + types.toString());

        debugLogging("====== Requesting DB for units...");
        Map<String, DictElemDTO> units = dictSrv.getMapDictElem(UNITS_DICT_CODE);
        notEmpty(units);
        debugLogging("======== DB response: " + units.toString());

        debugLogging("====== Requesting converting to PropertyFEDTO list...");
        List<PropertyFEDTO> result = convertPrpertyListToPropertyDtoList(propertyList, types, units);
        isNotEmptyAndNotNull(result);
        debugLogging("======== DB response: " + result.toString());

        debugLogging("==== end with result: " + result.toString());
        return result;

    }

    private void debugLogging(String msg) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(msg);
        }
    }

    private void isNotEmptyAndNotNull(List<PropertyFEDTO> result) {
        if (result == null || result.isEmpty())
            throw new EmptyOrNullResultException();
    }

    protected List<PropertyFEDTO> convertPrpertyListToPropertyDtoList(List<Property> propertyList, Map<String, DictElemDTO> types, Map<String, DictElemDTO> units) {
        return propertyList.stream()
                .map(property -> this.convertPropertyToPropertyFEDTO(types, units, property))
                .collect(toList());
    }

    protected PropertyFEDTO convertPropertyToPropertyFEDTO(Map<String, DictElemDTO> types,
                                                           Map<String, DictElemDTO> units,
                                                           Property property) {

        validateProperty(property);

        PropertyFEDTO propertyFEDTO = new PropertyFEDTO();
        propertyFEDTO.setActive(property.getActive());
        propertyFEDTO.setCode(property.getCode());
        propertyFEDTO.setDescription(property.getDescription());
        propertyFEDTO.setName(property.getName());
        propertyFEDTO.setValue(property.getValue());
        propertyFEDTO.setType(types.get(property.getType()));
        propertyFEDTO.setUnit(units.get(property.getUnit()));

        return propertyFEDTO;
    }

    private void validateProperty(Property property) {
        if (isEmpty(property.getCode())) {
            throw new IllegalArgumentException();
        }
        if (property.getActive() == null) {
            throw new IllegalArgumentException();
        }
        if (isEmpty(property.getType())) {
            throw new IllegalArgumentException();
        }
        if (isEmpty(property.getUnit())) {
            throw new IllegalArgumentException();
        }
        if (isEmpty(property.getValue())) {
            throw new IllegalArgumentException();
        }
    }

    public ResponseStatusDTO updateProperty(PropertyDTO propertyDTO) {
        propertiesDao.updateProperty(propertyDTO);
        return new ResponseStatusDTO("ok", "Resource changed", "Resource changed");
    }

    public int getValue(String code) {
        if (isEmpty(code))
            throw new IllegalArgumentException();

        return Integer.valueOf(propertiesDao.getValue(code));
    }

    public int getBaseUnitValue(String code) {
        if (isEmpty(code))
            throw new IllegalArgumentException();
        Property property = propertiesDao.getProperty(code);

        setUpUnitsMultiplier(property.getUnit());
        return Math.round(Float.valueOf(property.getValue()) * unitsMultiplier);

    }

    private void setUpUnitsMultiplier(String unitCode) {
        switch (unitCode) {
            case "PAUN_HOUR":
                unitsMultiplier = 3600000;
                break;
            case "PAUN_SEC":
                unitsMultiplier = 1000;
                break;
            case "PAUN_PLN":
                unitsMultiplier = 100;
                break;
            default:
                unitsMultiplier = 1;
                break;
        }
    }

}
