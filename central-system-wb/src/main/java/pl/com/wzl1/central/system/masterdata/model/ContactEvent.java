
/*
 ***********************************************************
 * SYSTEM CENTRALNY
 *
 * Autorskie Prawa Majątkowe:
 * Wojskowe Zakłady Łączności Nr 1 w Zegrzu (WZŁ1)
 ************************************************************
 * Autor: Piotr Rogowski
 * Data utworzenia: 20.09.2017 08:49:09
 * Wersja: 0.1
 ***********************************************************/

package pl.com.wzl1.central.system.masterdata.model;

import lombok.*;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ContactEvent {

    private String code;
    private String type;
    private String target;
}
