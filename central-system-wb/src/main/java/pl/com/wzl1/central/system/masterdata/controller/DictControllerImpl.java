/*
 ***********************************************************
 * SYSTEM CENTRALNY
 *
 * Autorskie Prawa Majątkowe:
 * Wojskowe Zakłady Łączności Nr 1 w Zegrzu (WZŁ1)
 ************************************************************
 * Autor: Piotr Rogowski
 * Data utworzenia: 08.08.2017 11:10:12
 * Wersja: 0.1
 ***********************************************************/

package pl.com.wzl1.central.system.masterdata.controller;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import pl.com.wzl1.central.system.dto.DictElemDTO;
import pl.com.wzl1.central.system.masterdata.service.DictSrvImpl;

import java.util.List;

@RestController
@Api(value = "Dictionary Resource")
@RequestMapping("/wzl/v1/dict")
public class DictControllerImpl {

    @Autowired
    private DictSrvImpl dictSrv;

    @ApiOperation(value = "List dictionary elements for code")
    @ApiResponses(value =
            {@ApiResponse(code = 200, message = "Successful", response = Boolean.class),
                    @ApiResponse(code = 500, message = "INSIDE SERVER ERROR")})
    @ApiImplicitParams({
            @ApiImplicitParam(name = "Authorization", value = "Authorization token",
                    required = true, dataType = "string", paramType = "header")
    })
    @HystrixCommand(commandKey = "DictControllerImpl.list")
    @RequestMapping(value = "/list/{smb}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.ALL_VALUE)
    public List<DictElemDTO> list(@PathVariable("smb") String codeDict) {
        return dictSrv.getListDictElem(codeDict);
    }

    @ApiOperation(value = "Map dictionary elements for code")
    @ApiResponses(value =
            {@ApiResponse(code = 200, message = "Successful", response = Boolean.class),
                    @ApiResponse(code = 500, message = "INSIDE SERVER ERROR")})
    @ApiImplicitParams({
            @ApiImplicitParam(name = "Authorization", value = "Authorization token",
                    required = true, dataType = "string", paramType = "header")
    })
    @HystrixCommand(commandKey = "DictControllerImpl.map")
    @RequestMapping(value = "/map/{smb}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.ALL_VALUE)
    public ResponseEntity<?> map(@PathVariable("smb") String codeDict) {
        return new ResponseEntity<>(dictSrv.getMapDictElem(codeDict), HttpStatus.OK);
    }

    @ApiOperation(value = "Get dictionary element for code")
    @ApiResponses(value =
            {@ApiResponse(code = 200, message = "Successful", response = Boolean.class),
                    @ApiResponse(code = 500, message = "INSIDE SERVER ERROR")})
    @ApiImplicitParams({
            @ApiImplicitParam(name = "Authorization", value = "Authorization token",
                    required = true, dataType = "string", paramType = "header")
    })
    @HystrixCommand(commandKey = "DictControllerImpl.dictElem")
    @RequestMapping(value = "/dict_elem/{code}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.ALL_VALUE)
    public DictElemDTO dictElem(@PathVariable("code") String dictElem) {
        return dictSrv.getDictElem(dictElem);
    }

    @ApiOperation(value = "List all dictionary")
    @ApiResponses(value =
            {@ApiResponse(code = 200, message = "Successful", response = Boolean.class),
                    @ApiResponse(code = 500, message = "INSIDE SERVER ERROR")})
    @ApiImplicitParams({
            @ApiImplicitParam(name = "Authorization", value = "Authorization token",
                    required = true, dataType = "string", paramType = "header")
    })
    @HystrixCommand(commandKey = "DictControllerImpl.listAll")
    @RequestMapping(value = "/all", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.ALL_VALUE)
    public ResponseEntity<?> listAll() {
        return new ResponseEntity<>(dictSrv.getDictAll(), HttpStatus.OK);
    }
}
