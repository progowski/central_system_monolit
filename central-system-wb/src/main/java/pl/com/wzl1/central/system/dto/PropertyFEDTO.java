package pl.com.wzl1.central.system.dto;

import lombok.Data;

@Data
public class PropertyFEDTO {

    String name;

    String value;

    String description;

    DictElemDTO type;

    DictElemDTO unit;

    String code;

    Boolean active;
}
