
/*
 ***********************************************************
 * SYSTEM CENTRALNY
 *
 * Autorskie Prawa Majątkowe:
 * Wojskowe Zakłady Łączności Nr 1 w Zegrzu (WZŁ1)
 ************************************************************
 * Autor: Piotr Rogowski
 * Data utworzenia: 10.08.2017 13:15:01
 * Wersja: 0.1
 ***********************************************************/

package pl.com.wzl1.central.system.masterdata.service;

import org.apache.commons.lang.Validate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.com.wzl1.central.system.dto.DictDTO;
import pl.com.wzl1.central.system.dto.DictElemDTO;
import pl.com.wzl1.central.system.masterdata.dao.DictDaoImpl;
import pl.com.wzl1.central.system.masterdata.model.Dictionary;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.springframework.util.ObjectUtils.isEmpty;

@Service
public class DictSrvImpl {

    @Autowired
    private DictDaoImpl dictDao;

    public List<DictElemDTO> getListDictElem(String codeDict) {
        return getListDictElemFromDao(codeDict);
    }

    public Map<String, DictElemDTO> getMapDictElem(String codeDict) {

        List<DictElemDTO> list = getListDictElemFromDao(codeDict);

        Map<String, DictElemDTO> map = new HashMap<String, DictElemDTO>();

        for (DictElemDTO dictElemDTO : list) {
            map.put(dictElemDTO.getCode(), dictElemDTO);
        }

        return map;
    }

    private List<DictElemDTO> getListDictElemFromDao(String codeDict) {
        if (codeDict != null) {
            List<DictElemDTO> list = dictDao.getListDictElem(codeDict);
            if (!isEmpty(list)) {
                Validate.notEmpty(list);
                return list;
            }
        }
        return null;
    }

    public DictElemDTO getDictElem(String dictElemCode) {
        if (dictElemCode != null) {
            DictElemDTO dto = dictDao.getDictElem(dictElemCode);
            if(dto != null){
                dto.setCode(dictElemCode);
                return dto;
            }
        }
        return null;
    }

    public List<DictDTO> getDictAll() {

        List<Dictionary> list = dictDao.getAll();

        if (!isEmpty(list)) {

            List<DictDTO> listDictDto = new ArrayList<>();

            for (Dictionary d : list) {
                DictDTO dictDto = DictDTO.builder().
                        code(d.getDictCode()).build();

                if (!listDictDto.contains(dictDto)) {
                    dictDto.setValue(d.getDictName());
                    dictDto.setDescription(d.getDictDescription());
                    dictDto.setListDictElemDTO(new ArrayList<>());
                    listDictDto.add(dictDto);
                }

                DictElemDTO elemDto = DictElemDTO.builder()
                        .code(d.getDictElemCode())
                        .value(d.getDictElemName())
                        .description(d.getDictElemDescription())
                        .active(d.isDictElemActive())
                        .dateCre(d.getDictElemDateCre())
                        .build();

                listDictDto.get(listDictDto.indexOf(dictDto)).getListDictElemDTO().add(elemDto);
            }

            return listDictDto;
        }

        return null;
    }

}
