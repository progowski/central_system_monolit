
/*
***********************************************************
 * SYSTEM CENTRALNY
 * 
 * Autorskie Prawa Majątkowe:
 * Wojskowe Zakłady Łączności Nr 1 w Zegrzu (WZŁ1)
 ************************************************************
 * Autor: Piotr Rogowski 
 * Data utworzenia: 01.09.2017 08:12:32
 * Wersja: 0.1
 ***********************************************************/

package pl.com.wzl1.central.system.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.util.List;

@Data
public class EventDTO {

    private String code;
    private String name;
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private String description;
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private String priorityCode;
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private String priorityName;
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private String typeCode;
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private String typeName;
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private String colorCode;
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private String colorName;
    private boolean sound;
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private Integer lifeTime;
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    List<EventMessageDTO> messages;
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private Boolean top;

    public EventDTO(String code, String priorityCode, String typeCode, String colorCode, boolean sound, Integer lifeTime, Boolean top) {
        this.code = code;
        this.priorityCode = priorityCode;
        this.typeCode = typeCode;
        this.colorCode = colorCode;
        this.sound = sound;
        this.lifeTime = lifeTime;
        this.top = top;
    }

    public EventDTO() {
    }

    @Override
    public String toString() {
        return "EventDTO [code=" + code + ", name=" + name + ", description=" + description + ", priorityCode="
                        + priorityCode + ", priorityName=" + priorityName + ", typeCode=" + typeCode + ", typeName="
                        + typeName + ", colorCode=" + colorCode + ", colorName=" + colorName + ", sound=" + sound
                        + ", lifeTime=" + lifeTime + ", messages=" + messages + ", top=" + top + "]";
    }
    
    

}
