package pl.com.wzl1.central.system.masterdata.model;

import lombok.Data;

/*
 *  ************************************************************
 *  * SYSTEM CENTRALNY
 *  *
 *  * Autorskie Prawa Majatkowe:
 *  * Wojskowe Zaklady Lacznosci Nr 1 w Zegrzu (WZL1)
 *  ************************************************************
 *  * Autor: Artur Kowalski
 *  * Data utworzenia: 07.08.2017 12:17
 *  * Wersja: 0.1
 *  ************************************************************
 *
 */
@Data
public class PropertyValue {

    String value;

    public PropertyValue(int value) {
        this.value = String.valueOf(value);
    }
}
