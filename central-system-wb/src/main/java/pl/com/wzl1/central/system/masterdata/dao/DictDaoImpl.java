
/*
 ***********************************************************
 * SYSTEM CENTRALNY
 *
 * Autorskie Prawa Majątkowe:
 * Wojskowe Zakłady Łączności Nr 1 w Zegrzu (WZŁ1)
 ************************************************************
 * Autor: Piotr Rogowski
 * Data utworzenia: 10.08.2017 13:13:47
 * Wersja: 0.1
 ***********************************************************/

package pl.com.wzl1.central.system.masterdata.dao;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;
import pl.com.wzl1.central.system.dto.DictElemDTO;
import pl.com.wzl1.central.system.masterdata.model.Dictionary;
import pl.com.wzl1.central.system.masterdata.sql.DictSQL;


import java.sql.Types;
import java.util.List;

@Repository
public class DictDaoImpl {

    private final NamedParameterJdbcTemplate jdbcTemplate;

    public DictDaoImpl(NamedParameterJdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public List<DictElemDTO> getListDictElem(String codeDict) {
        MapSqlParameterSource mapParams = new MapSqlParameterSource();
        mapParams.addValue("dict_smb", codeDict, Types.VARCHAR);

        return jdbcTemplate.query(DictSQL.LIST_DICT_ELEM, mapParams,
                new BeanPropertyRowMapper<DictElemDTO>(DictElemDTO.class));
    }

    public DictElemDTO getDictElem(String codeDictElem) {
        MapSqlParameterSource mapParams = new MapSqlParameterSource();
        mapParams.addValue("dict_elem_code", codeDictElem, Types.VARCHAR);
        try {
            return jdbcTemplate.queryForObject(DictSQL.DICT_ELEM, mapParams,
                    new BeanPropertyRowMapper<DictElemDTO>(DictElemDTO.class));
        } catch (org.springframework.dao.EmptyResultDataAccessException e) {
            return null;
        }
    }

    public List<Dictionary> getAll() {
        List<Dictionary> list = jdbcTemplate.query(DictSQL.LIST_DICT_ALL,
                new BeanPropertyRowMapper<Dictionary>(Dictionary.class));
        return list;
    }

}
