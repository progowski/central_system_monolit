
/*
 ***********************************************************
 * SYSTEM CENTRALNY
 *
 * Autorskie Prawa Majątkowe:
 * Wojskowe Zakłady Łączności Nr 1 w Zegrzu (WZŁ1)
 ************************************************************
 * Autor: Piotr Rogowski
 * Data utworzenia: 01.09.2017 08:54:06
 * Wersja: 0.1
 ***********************************************************/


package pl.com.wzl1.central.system.masterdata.controller;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import io.swagger.annotations.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.com.wzl1.central.system.dto.ContactDTO;
import pl.com.wzl1.central.system.dto.EventDTO;
import pl.com.wzl1.central.system.masterdata.service.EventSrvImpl;

import java.security.Principal;
import java.util.List;

@RestController
@RequestMapping("/wzl/v1/conf/event")
public class EventControllerImpl {

    private final static Logger Logger = LogManager.getLogger(EventControllerImpl.class);
    @Autowired
    private EventSrvImpl eventSrv;

    @ApiOperation(value = "List events")
    @ApiResponses(value =
            {@ApiResponse(code = 200, message = "Successful", response = Boolean.class),
                    @ApiResponse(code = 500, message = "INSIDE SERVER ERROR")})
    @ApiImplicitParams({
            @ApiImplicitParam(name = "Authorization", value = "Authorization token",
                    required = true, dataType = "string", paramType = "header")
    })
    @HystrixCommand(commandKey = "EventControllerImpl.list")
    @RequestMapping(value = "/list", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.ALL_VALUE)
    public ResponseEntity<?> list() {
        Logger.info("START");
        List<EventDTO> listDTO = eventSrv.getEvents();

        if (Logger.isDebugEnabled()) {
            Logger.debug("RETURN LIST: {}", listDTO);
        }

        Logger.info("END");

        return new ResponseEntity<>(listDTO, HttpStatus.OK);
    }

    @ApiOperation(value = "Get event configuration properties")
    @ApiResponses(value =
            {@ApiResponse(code = 200, message = "Successful", response = Boolean.class),
                    @ApiResponse(code = 500, message = "INSIDE SERVER ERROR")})
    @ApiImplicitParams({
            @ApiImplicitParam(name = "Authorization", value = "Authorization token",
                    required = true, dataType = "string", paramType = "header")
    })
    @HystrixCommand(commandKey = "EventControllerImpl.event")
    @RequestMapping(value = "/{code}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.ALL_VALUE)
    public ResponseEntity<?> event(@PathVariable String code) {
        Logger.info("START");

        if (Logger.isDebugEnabled()) {
            Logger.debug("INPUT PARAMS: code={}", code);
        }
        EventDTO dto = eventSrv.getProperties(code);

        if (Logger.isDebugEnabled()) {
            Logger.debug("RETURN OBJECT: {}", dto);
        }

        Logger.info("END");
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }

    @ApiOperation(value = "Edit event configuration properties")
    @ApiResponses(value =
            {@ApiResponse(code = 200, message = "Successful", response = Boolean.class),
                    @ApiResponse(code = 400, message = "EMPTY DTO, EMPTY EVENT CODE, EMPTY EMAIL OR TELEPHONE NUMBER, " +
                            "INVALID EMAIL, INVALID TELEPHONE NUMBER", response = Boolean.class),
                    @ApiResponse(code = 500, message = "INSIDE SERVER ERROR")})
    @ApiImplicitParams({
            @ApiImplicitParam(name = "Authorization", value = "Authorization token",
                    required = true, dataType = "string", paramType = "header")
    })
    @HystrixCommand(commandKey = "EventControllerImpl.edit")
    @RequestMapping(method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.ALL_VALUE)
    public void edit(@RequestBody EventDTO dto, Principal principal) {
        Logger.info("START");

        String userExecute = getCurrentUser(principal).getName();

        if (Logger.isDebugEnabled()) {
            Logger.debug("INPUT PARAMS: userExecute={}, dto={}", userExecute, dto);
        }

        eventSrv.editEvent(dto, userExecute);
    }

    @ApiOperation(value = "List all events")
    @ApiResponses(value =
            {@ApiResponse(code = 200, message = "Successful", response = Boolean.class),
                    @ApiResponse(code = 500, message = "INSIDE SERVER ERROR")})
    @ApiImplicitParams({
            @ApiImplicitParam(name = "Authorization", value = "Authorization token",
                    required = true, dataType = "string", paramType = "header")
    })
    @HystrixCommand(commandKey = "EventControllerImpl.listAll")
    @RequestMapping(value = "/list/all", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.ALL_VALUE)
    public ResponseEntity<?> listAll() {
        Logger.info("START");
        List<EventDTO> listDTO = eventSrv.getEventsAllInfo();

        if (Logger.isDebugEnabled()) {
            Logger.debug("RETURN LIST: {}", listDTO);
        }

        Logger.info("END");

        return new ResponseEntity<>(listDTO, HttpStatus.OK);
    }

    @ApiOperation(value = "List contacts")
    @ApiResponses(value =
            {@ApiResponse(code = 200, message = "Successful", response = Boolean.class),
                    @ApiResponse(code = 500, message = "INSIDE SERVER ERROR")})
    @ApiImplicitParams({
            @ApiImplicitParam(name = "Authorization", value = "Authorization token",
                    required = true, dataType = "string", paramType = "header")
    })
    @HystrixCommand(commandKey = "EventControllerImpl.listContacts")
    @RequestMapping(value = "/contacts", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.ALL_VALUE)
    public ResponseEntity<?> listContacts() {
        Logger.info("START");

        List<ContactDTO> listDTO = eventSrv.getContactList();

        if (Logger.isDebugEnabled()) {
            Logger.debug("RETURN listDTO: {}", listDTO);
        }

        Logger.info("END");

        return new ResponseEntity<>(listDTO, HttpStatus.OK);
    }

    private Principal getCurrentUser(Principal currentUser) {
        if (currentUser == null) {
            currentUser = () -> "admin";
        }
        return currentUser;
    }
}
