package pl.com.wzl1.central.system.commons;

import org.springframework.stereotype.Service;

import java.time.Instant;
import java.time.LocalDateTime;

@Service
public class TimeService {

    public Long getTime() {
        return Instant.now().toEpochMilli();
    }

    public LocalDateTime getLocalDateTime (){
        return LocalDateTime.now();
    }

}
