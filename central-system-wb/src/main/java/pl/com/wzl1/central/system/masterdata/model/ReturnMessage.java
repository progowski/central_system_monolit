
/************************************************************
 * SYSTEM CENTRALNY
 *
 * Autorskie Prawa Majątkowe:
 * Wojskowe Zakłady Łączności Nr 1 w Zegrzu (WZŁ1)
 ************************************************************
 * Autor: Piotr Rogowski 
 * Data utworzenia: 2017-07-14
 * Wersja: 0.1
 ************************************************************/

package pl.com.wzl1.central.system.masterdata.model;

public class ReturnMessage {

    private Integer status;
    private String code;
    private String message;
    private boolean success;

    public ReturnMessage() {
        super();
    }

    public ReturnMessage(Integer status, String code, String message) {
        super();
        this.status = status;
        this.code = code;
        this.message = message;
    }

    public ReturnMessage(Integer status, String code, String message, boolean success) {
        super();
        this.status = status;
        this.code = code;
        this.message = message;
        this.success = success;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

}
