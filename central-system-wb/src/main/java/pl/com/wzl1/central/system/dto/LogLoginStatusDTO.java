package pl.com.wzl1.central.system.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import pl.com.wzl1.central.system.commons.enums.LoginStatusEnum;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class LogLoginStatusDTO {

    private LoginStatusEnum status;
    private Long blockedTo;
}

