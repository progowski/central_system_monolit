package pl.com.wzl1.central.system.dto;

public class PropertyDictFieldDTO {

    private String code;

    private String value;

    public PropertyDictFieldDTO(String code, String value) {
        this.code = code;
        this.value = value;
    }

    @Override
    public String toString() {
        return "PropertyDictFieldDTO{" +
                "code='" + code + '\'' +
                ", value='" + value + '\'' +
                '}';
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
