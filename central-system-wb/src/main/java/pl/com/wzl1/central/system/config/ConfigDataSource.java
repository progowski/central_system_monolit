package pl.com.wzl1.central.system.config;

import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import pl.com.wzl1.central.system.services.HelloWorldService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import java.sql.ResultSet;
import java.util.Hashtable;

@Configuration
public class ConfigDataSource {

    private static final String JNDI_MASTERDATA = "jdbc/masterdata";

    private static final Logger LOGGER = LoggerFactory.getLogger(ConfigDataSource.class);

    @Bean
    NamedParameterJdbcTemplate jdbcTemplate() throws NamingException {

        Hashtable<String, String> ht = new Hashtable();
        ht.put(Context.INITIAL_CONTEXT_FACTORY,  "weblogic.jndi.WLInitialContextFactory");
        ht.put(Context.PROVIDER_URL, "t3://localhost:7003");

        Context ctx = new InitialContext(ht);
        DataSource dataSource = (DataSource)  ctx.lookup(JNDI_MASTERDATA);

        return new NamedParameterJdbcTemplate(dataSource);
    }

}
