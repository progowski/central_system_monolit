package pl.com.wzl1.central.system.dto;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import lombok.Data;

/*
 *  ************************************************************
 *  * SYSTEM CENTRALNY
 *  *
 *  * Autorskie Prawa Majatkowe:
 *  * Wojskowe Zaklady Lacznosci Nr 1 w Zegrzu (WZL1)
 *  ************************************************************
 *  * Autor: Artur Kowalski
 *  * Data utworzenia: 02.08.2017 14:28
 *  * Wersja: 0.1
 *  ************************************************************
 *
 */

@JsonAutoDetect
@Data
public class PropertyDTO {


    String name;

    String value;

    String description;

    String type;

    String unit;

    String code;

    Boolean active;

    public PropertyDTO() {
    }

    public PropertyDTO(String name, String value, String description, String type, String unit, String code, Boolean active) {

        this.name = name;
        this.value = value;
        this.description = description;
        this.type = type;
        this.unit = unit;
        this.code = code;
        this.active = active;
    }
}
