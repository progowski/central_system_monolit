
/************************************************************
 * SYSTEM CENTRALNY
 *
 * Autorskie Prawa Majątkowe:
 * Wojskowe Zakłady Łączności Nr 1 w Zegrzu (WZŁ1)
 ************************************************************
 * Autor: Rafał Karczmarz
 * Data utworzenia: 13 lip 2017
 * Wersja: 0.1
 ************************************************************/

package pl.com.wzl1.central.system.dto;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;


@Data
@NoArgsConstructor
@AllArgsConstructor

@JsonAutoDetect
public class ResponseStatusDTO implements Serializable {

    private String status;

    private String code;

    private String description;

    public ResponseStatusDTO(ResponseException responseException) {
        this.status = responseException.getStatus();
        this.code = responseException.getCode();
        this.description = responseException.getDescription();
    }

    public ResponseStatusDTO(String status) {
        this.status = status;
    }
}