package pl.com.wzl1.central.system.masterdata.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(of = {"code"})
public class DictElem {

    private String code;
    private String value;
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private String description;
}