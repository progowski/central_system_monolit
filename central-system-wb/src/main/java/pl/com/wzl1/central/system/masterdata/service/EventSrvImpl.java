
/*
 ***********************************************************
 * SYSTEM CENTRALNY
 *
 * Autorskie Prawa Majątkowe:
 * Wojskowe Zakłady Łączności Nr 1 w Zegrzu (WZŁ1)
 ************************************************************
 * Autor: Piotr Rogowski
 * Data utworzenia: 01.09.2017 08:32:00
 * Wersja: 0.1
 ***********************************************************/

package pl.com.wzl1.central.system.masterdata.service;

import org.apache.commons.validator.routines.EmailValidator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.com.wzl1.central.system.commons.CentralSystemBusinessException;
import pl.com.wzl1.central.system.commons.dto.validator.PhoneValidator;
import pl.com.wzl1.central.system.dto.ContactDTO;
import pl.com.wzl1.central.system.dto.EventDTO;
import pl.com.wzl1.central.system.dto.EventMessageDTO;
import pl.com.wzl1.central.system.masterdata.dao.EventsDaoImpl;
import pl.com.wzl1.central.system.masterdata.model.ContactEvent;
import pl.com.wzl1.central.system.masterdata.model.Event;
import pl.com.wzl1.central.system.masterdata.model.EventMessage;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static org.springframework.util.ObjectUtils.isEmpty;

@Service
public class EventSrvImpl {

    private final static Logger LOGGER = LogManager.getLogger(EventSrvImpl.class);
    @Autowired
    private EventsDaoImpl eventsDao;

    public List<EventDTO> getEvents() {
        List<Event> eventList = eventsDao.getEvents();
        List<EventDTO> eventDTOList = new ArrayList<>();

        if (!isEmpty(eventList)) {
            for (Event e : eventList) {
                EventDTO dto = new EventDTO();
                BeanUtils.copyProperties(e, dto);
                eventDTOList.add(dto);
            }
        }
        return eventDTOList;

    }

    public EventDTO getProperties(String codeEvent) {
        if (codeEvent == null) {
            return null;
        }
        Event event = eventsDao.getProperties(codeEvent);

        if (event == null) {
            return null;
        }

        EventDTO dto = new EventDTO();

        BeanUtils.copyProperties(event, dto);

        if (event.isTop() != null) {
            dto.setTop(event.isTop());
        }

        List<EventMessage> messagesList = eventsDao.getMessages(codeEvent);

        if (!isEmpty(messagesList)) {
            List<EventMessageDTO> messagesListDTO = new ArrayList<>();

            for (EventMessage em : messagesList) {
                EventMessageDTO messageDTO = new EventMessageDTO();
                BeanUtils.copyProperties(em, messageDTO);
                messagesListDTO.add(messageDTO);
            }

            dto.setMessages(messagesListDTO);

        }
        return dto;
    }

    @Transactional(rollbackFor = {Exception.class})
    public void editEvent(EventDTO dto, String userEdit) {
        if (dto == null) {
            throw new CentralSystemBusinessException("event_manage.edit.error.empty.dto", "EMPTY DTO");
        }
        if (dto.getCode() == null) {
            throw new CentralSystemBusinessException("event_manage.edit.error.code.empty", "EMPTY EVENT CODE");
        }

        if (isContactsModified(dto.getCode(), dto.getMessages())) {
            eventsDao.setDateModForEvent(dto.getCode(), userEdit);
        }

        if (isEmpty(dto.getMessages())) {
            eventsDao.deactivateAllMessages(dto.getCode(), userEdit);
        } else {
            Set<String> targets = dto.getMessages().stream().map(m -> m.getTarget()).collect(Collectors.toSet());
            eventsDao.deactivateMessages(targets, dto.getCode(), userEdit);
        }


        List<EventMessageDTO> messagesListDTO = dto.getMessages();

        if (!isEmpty(messagesListDTO)) {

            EmailValidator emailValidator = EmailValidator.getInstance();

            if (messagesListDTO.stream().anyMatch(x -> (x.getType() == null || x.getTarget() == null || x.getTarget()
                    .trim().equals("")))) {
                throw new CentralSystemBusinessException("event_manage.edit.error.contact.empty", "EMPTY EMAIL OR TELEPHONE NUMBER");
            }

            if (messagesListDTO.stream().anyMatch(x -> x.getType().equals("EMAIL") && !emailValidator.isValid(x
                    .getTarget()))) {
                throw new CentralSystemBusinessException("event_manage.edit.error.email.invalid", "INVALID EMAIL");
            }

            if (messagesListDTO.stream().anyMatch(x -> x.getType().equals("SMS") && !PhoneValidator.validatePhoneNumber(x
                    .getTarget()))) {
                throw new CentralSystemBusinessException("event_manage.edit.error.telephone.invalid", "INVALID TELEPHONE NUMBER");
            }

            eventsDao.addMessages(messagesListDTO, dto.getCode(), userEdit);
        }

        eventsDao.updateEvent(dto, userEdit);
    }

    public List<EventDTO> getEventsAllInfo() {
        List<Event> eventList = eventsDao.getEventsAllInfo();
        List<EventDTO> eventDTOList = new ArrayList<>();

        if (!isEmpty(eventList)) {
            for (Event e : eventList) {
                EventDTO dto = new EventDTO();
                BeanUtils.copyProperties(e, dto);
                if (e.isTop() != null) {
                    dto.setTop(e.isTop());
                }
                eventDTOList.add(dto);
            }
        }

        return eventDTOList;
    }

    public List<ContactDTO> getContactList() {
        List<ContactEvent> contactList = eventsDao.getContactEventList();
        List<ContactDTO> contactListDTO = null;

        if (!isEmpty(contactList)) {
            contactListDTO = new ArrayList<>();
            for (ContactEvent ce : contactList) {
                ContactDTO dto = new ContactDTO();
                BeanUtils.copyProperties(ce, dto);
                contactListDTO.add(dto);
            }
        }
        return contactListDTO;
    }

    public boolean isContactsModified(String codeEvent, List<EventMessageDTO> messagesListDTO) {

        List<EventMessage> listFromDB = eventsDao.getMessages(codeEvent);

        int listFromDBSize = listFromDB != null ? listFromDB.size() : 0;
        int messagesListDTOSize = messagesListDTO != null ? messagesListDTO.size() : 0;

        if (listFromDBSize != messagesListDTOSize) {
            return true;
        }

        if (listFromDBSize == 0 && messagesListDTOSize == 0) {
            return false;
        }

        for (EventMessageDTO dto : messagesListDTO) {
            EventMessage em = new EventMessage();
            BeanUtils.copyProperties(dto, em);
            if (!listFromDB.contains(em)) {
                return true;
            }
        }

        return false;
    }

}
