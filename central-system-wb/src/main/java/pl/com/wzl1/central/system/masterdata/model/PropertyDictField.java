package pl.com.wzl1.central.system.masterdata.model;

import lombok.Data;

@Data
public class PropertyDictField {

    private String code;

    private String value;

    public PropertyDictField(String code, String value) {
        this.code = code;
        this.value = value;
    }
}
