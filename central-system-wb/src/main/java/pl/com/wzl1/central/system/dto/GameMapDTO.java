package pl.com.wzl1.central.system.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class GameMapDTO {
    private Integer id;
    private Integer devId;
    private String ver;
}
