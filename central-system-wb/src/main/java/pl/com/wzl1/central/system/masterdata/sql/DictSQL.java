
/*
 ***********************************************************
 * SYSTEM CENTRALNY
 *
 * Autorskie Prawa Majątkowe:
 * Wojskowe Zakłady Łączności Nr 1 w Zegrzu (WZŁ1)
 ************************************************************
 * Autor: Piotr Rogowski
 * Data utworzenia: 10.08.2017 13:14:02
 * Wersja: 0.1
 ***********************************************************/

package pl.com.wzl1.central.system.masterdata.sql;

public class DictSQL {

    public static final String LIST_DICT_ELEM = "SELECT "
            + "DE.CODE code, "
            + "DE.VALUE value "
            + "FROM DICT_ELEM DE "
            + "WHERE DE.SMB = :dict_smb";

    public static final String DICT_ELEM = "SELECT "
            + "DE.VALUE value, "
            + "DE.DESCR description "
            + "FROM DICT_ELEM DE "
            + "WHERE DE.CODE = :dict_elem_code";

    public static final String LIST_DICT_ALL = "SELECT "
            + "D.SMB dictCode,"
            + "D.NAME dictName,"
            + "D.DESCR dictDescription,"
            + "DE.CODE dictElemCode,"
            + "DE.VALUE dictElemName,"
            + "DE.DESCR dictElemDescription,"
            + "DE.ACTIVE dictElemActive,"
            + "DE.DATE_CRE dictElemDateCre "
            + "FROM DICT D "
            + "JOIN DICT_ELEM DE ON DE.SMB = D.SMB "
            + "ORDER BY D.SMB,DE.CODE";

}
