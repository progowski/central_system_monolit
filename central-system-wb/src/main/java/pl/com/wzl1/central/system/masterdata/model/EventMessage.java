
/*
 ***********************************************************
 * SYSTEM CENTRALNY
 *
 * Autorskie Prawa Majątkowe:
 * Wojskowe Zakłady Łączności Nr 1 w Zegrzu (WZŁ1)
 ************************************************************
 * Autor: Piotr Rogowski
 * Data utworzenia: 01.09.2017 10:49:24
 * Wersja: 0.1
 ***********************************************************/

package pl.com.wzl1.central.system.masterdata.model;

public class EventMessage {

    private String type;
    private String target;

    public EventMessage() {
    }

    public EventMessage(String type, String target) {
        this.type = type;
        this.target = target;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    @Override
    public String toString() {
        return "EventMessage{" +
                "type='" + type + '\'' +
                ", target='" + target + '\'' +
                '}';
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((target == null) ? 0 : target.hashCode());
        result = prime * result + ((type == null) ? 0 : type.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        EventMessage other = (EventMessage) obj;
        if (target == null) {
            if (other.target != null)
                return false;
        } else if (!target.equals(other.target))
            return false;
        if (type == null) {
            if (other.type != null)
                return false;
        } else if (!type.equals(other.type))
            return false;
        return true;
    }


}
