package pl.com.wzl1.central.system.config;

import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("pl.com.wzl1.central.system")
public class ConfigBeans {

    @Bean
    public ModelMapper modelMapper() {
        return new ModelMapper();
    }
}
