package pl.com.wzl1.central.system.commons;

import lombok.Value;

@Value
public class CentralSystemServerException extends RuntimeException {

    private final String messageCode;
    private final String message;

}
