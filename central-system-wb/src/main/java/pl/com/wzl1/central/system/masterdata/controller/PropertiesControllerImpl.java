package pl.com.wzl1.central.system.masterdata.controller;

/*
 *  ************************************************************
 *  * SYSTEM CENTRALNY
 *  *
 *  * Autorskie Prawa Majatkowe:
 *  * Wojskowe Zaklady Lacznosci Nr 1 w Zegrzu (WZL1)
 *  ************************************************************
 *  * Autor: Artur Kowalski
 *  * Data utworzenia: 23.08.2017 15:06
 *  * Wersja: 0.1
 *  ************************************************************
 *
 */

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import pl.com.wzl1.central.system.commons.ObjectToDtoConverter;
import pl.com.wzl1.central.system.dto.*;
import pl.com.wzl1.central.system.masterdata.model.PropertyValue;
import pl.com.wzl1.central.system.masterdata.service.DictSrvImpl;
import pl.com.wzl1.central.system.masterdata.service.PropertiesSrvImpl;

import java.util.List;

@RestController
@RequestMapping("/wzl/v1")
public class PropertiesControllerImpl {

    protected final static String TYPES_DICT_CODE = "PATP";

    protected final static String UNITS_DICT_CODE = "PAUN";
    private final static Logger LOGGER = LogManager.getLogger(PropertiesControllerImpl.class);
    private final PropertiesSrvImpl propertiesSrv;
    private final DictSrvImpl dictSrv;

    private final ObjectToDtoConverter objectToDtoConverter;

    public PropertiesControllerImpl(PropertiesSrvImpl propertiesSrv, DictSrvImpl dictSrv, ObjectToDtoConverter objectToDtoConverter) {
        this.propertiesSrv = propertiesSrv;
        this.dictSrv = dictSrv;
        this.objectToDtoConverter = objectToDtoConverter;
    }

    @HystrixCommand(commandKey = "PropertiesControllerImpl.listAllProperties")
    @RequestMapping(value = "/properties", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.ALL_VALUE)
    public List<PropertyFEDTO> listAllProperties() {
        LOGGER.info("== Listing properties");
        return propertiesSrv.getAllProperties();

    }

    @HystrixCommand(commandKey = "PropertiesControllerImpl.updateProperty")
    @RequestMapping(value = "properties/property", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.ALL_VALUE)
    public ResponseStatusDTO updateProperty(@RequestBody @NotEmpty PropertyDTO propertyDTO) {

        LOGGER.info("== Editing properties");
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("==== Recived params: " + propertyDTO.toString());
        }
        return propertiesSrv.updateProperty(propertyDTO);
    }

    @HystrixCommand(commandKey = "PropertiesControllerImpl.getValue")
    @RequestMapping(value = "properties/property/baseUnit/{code}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.ALL_VALUE)
    public PropertyValueDTO getBaseUnitValue(@PathVariable @NotEmpty String code) {

        LOGGER.info("== Getting property value");
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("==== Recived params: code " + code);
        }
        PropertyValueDTO propertyValueDTO = (PropertyValueDTO) objectToDtoConverter.convertToDto(new PropertyValue(propertiesSrv.getBaseUnitValue(code)), PropertyValueDTO.class);

        return propertyValueDTO;

    }

    @HystrixCommand(commandKey = "PropertiesControllerImpl.getValue")
    @RequestMapping(value = "properties/property/{code}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.ALL_VALUE)
    public PropertyValueDTO getValue(@PathVariable @NotEmpty String code) {

        LOGGER.info("== Getting property value");
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("==== Recived params: code " + code);
        }
        PropertyValueDTO propertyValueDTO = (PropertyValueDTO) objectToDtoConverter.convertToDto(new PropertyValue(propertiesSrv.getValue(code)), PropertyValueDTO.class);
        return propertyValueDTO;

    }

    @HystrixCommand(commandKey = "PropertiesControllerImpl.getTypes")
    @RequestMapping(value = "properties/types", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.ALL_VALUE)
    public List<DictElemDTO> getTypes() {

        LOGGER.info("== Getting property types list start");
        return dictSrv.getListDictElem(TYPES_DICT_CODE);
    }

    @HystrixCommand(commandKey = "PropertiesControllerImpl.getUnits")
    @RequestMapping(value = "properties/units", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.ALL_VALUE)
    public List<DictElemDTO> getUnits() {

        LOGGER.info("== Getting property units list start");
        return dictSrv.getListDictElem(UNITS_DICT_CODE);

    }

}
