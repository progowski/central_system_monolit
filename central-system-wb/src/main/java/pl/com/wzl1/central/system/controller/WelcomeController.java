package pl.com.wzl1.central.system.controller;

import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import pl.com.wzl1.central.system.dto.ExampleDTO;
import pl.com.wzl1.central.system.services.HelloWorldService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
public class WelcomeController {

	private final Logger logger = LoggerFactory.getLogger(WelcomeController.class);
	private final HelloWorldService helloWorldService;

	@Autowired
	public WelcomeController(HelloWorldService helloWorldService) {
		this.helloWorldService = helloWorldService;
	}
	
	@RequestMapping(value = "/testing", method = RequestMethod.GET)
	public String testing() {
		return "testing";
	}

	@Autowired
	private NamedParameterJdbcTemplate jdbcTemplate;

	@RequestMapping(value = "/dto",method = RequestMethod.GET,produces = {MediaType.APPLICATION_JSON_VALUE})
	@ResponseBody
	public ExampleDTO dto() {
		ExampleDTO dto = new ExampleDTO();
		dto.setId(1);
		dto.setName("aaaa");
		return dto;
	}
	
	@GetMapping(value="/dtos")
	@ResponseBody
	public ExampleDTO dtos() {

		Map<String,Object> maps = jdbcTemplate.queryForMap("SELECT * FROM DICT",new MapSqlParameterSource());

		System.out.println("maps = "+maps);

		logger.info("maps = "+maps);

		ExampleDTO dto = new ExampleDTO();
		dto.setId(1);
		dto.setName("aaaa");
		return dto;
	}
}