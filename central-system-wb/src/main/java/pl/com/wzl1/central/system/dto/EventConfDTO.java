
/*
***********************************************************
 * SYSTEM CENTRALNY
 * 
 * Autorskie Prawa Majątkowe:
 * Wojskowe Zakłady Łączności Nr 1 w Zegrzu (WZŁ1)
 ************************************************************
 * Autor: Piotr Rogowski 
 * Data utworzenia: 07.09.2017 08:20:25
 * Wersja: 0.1
 ***********************************************************/

package pl.com.wzl1.central.system.dto;

public class EventConfDTO {

    private String dictEvnt;

    private EventDTO event;

    public EventConfDTO() {
        super();
    }

    public EventConfDTO(String dictEvnt, EventDTO event) {
        super();
        this.dictEvnt = dictEvnt;
        this.event = event;
    }

    public String getDictEvnt() {
        return dictEvnt;
    }

    public void setDictEvnt(String dictEvnt) {
        this.dictEvnt = dictEvnt;
    }

    public EventDTO getEvent() {
        return event;
    }

    public void setEvent(EventDTO event) {
        this.event = event;
    }

    @Override
    public String toString() {
        return "EventConfDTO [dictEvnt=" + dictEvnt + ", event=" + event + "]";
    }

}
