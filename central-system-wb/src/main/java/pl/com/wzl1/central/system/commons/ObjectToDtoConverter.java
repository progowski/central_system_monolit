package pl.com.wzl1.central.system.commons;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ObjectToDtoConverter {

    @Autowired
    private ModelMapper modelMapper;

    public Object convertToDto(Object entity, Class<?> dtoClass) {
        return modelMapper.map(entity, dtoClass);
    }
}
