package pl.com.wzl1.central.system.masterdata.model;

import lombok.Data;

import java.io.Serializable;

/*
 *  ************************************************************
 *  * SYSTEM CENTRALNY
 *  *
 *  * Autorskie Prawa Majatkowe:
 *  * Wojskowe Zaklady Lacznosci Nr 1 w Zegrzu (WZL1)
 *  ************************************************************
 *  * Autor: Artur Kowalski
 *  * Data utworzenia: 31.07.2017 07:55
 *  * Wersja: 0.1
 *  ************************************************************
 *
 */

@Data
public class Property implements Serializable {

    String name;

    String value;

    String description;

    String type;

    String unit;

    String code;

    Boolean active;

    public Property() {
    }

    public Property(String name, String value, String description, String type, String unit, String code, Boolean active) {
        this.name = name;
        this.value = value;
        this.description = description;
        this.type = type;
        this.unit = unit;
        this.code = code;
        this.active = active;
    }
}
