
/*
 ***********************************************************
 * SYSTEM CENTRALNY
 *
 * Autorskie Prawa Majątkowe:
 * Wojskowe Zakłady Łączności Nr 1 w Zegrzu (WZŁ1)
 ************************************************************
 * Autor: Piotr Rogowski
 * Data utworzenia: 16.08.2017 07:45:59
 * Wersja: 0.1
 ***********************************************************/

package pl.com.wzl1.central.system.masterdata.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Dictionary {

    private String dictCode;
    private String dictName;
    private String dictDescription;
    private String dictElemCode;
    private String dictElemName;
    private String dictElemDescription;
    private boolean dictElemActive;
    private LocalDateTime dictElemDateCre;

}
