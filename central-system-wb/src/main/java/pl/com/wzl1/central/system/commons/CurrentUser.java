package pl.com.wzl1.central.system.commons;


import lombok.Data;
import pl.com.wzl1.central.system.dto.PermissionDTO;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/*
 * ***********************************************************
 *  * SYSTEM CENTRALNY
 *  *
 *  * Autorskie Prawa Majątkowe:
 *  * Wojskowe Zakłady Łączności Nr 1 w Zegrzu (WZŁ1)
 *  ************************************************************
 *  * Autor: Rafał Karczmarz
 *  * Data utworzenia: 28.09.17 14:23
 *  * Wersja: 0.1
 *  ***********************************************************
 *
 */


//Class for take a user to try login
@Data
public class CurrentUser {

    private String login;

    private String name;

    private String surname;

    private Boolean active;

    private Boolean blocked;

    private Date blockedTo;

    private Boolean deleted;

    private String password;

    private String dictUsgr;

    private String depCode;

    List<PermissionDTO> permission = new ArrayList<>();

}
